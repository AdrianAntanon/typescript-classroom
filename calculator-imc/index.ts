const height: HTMLInputElement = document.querySelector("#height");
const weight: HTMLInputElement = document.querySelector("#weight");

const imc = (weight: number, height: number): String => {
  const imc = ((weight * 100) / height ** 2) * 100;

  if (isNaN(weight) || isNaN(height)) {
    return "Introduce los datos correctamente, por favor";
  }

  let answer: string;
  let num: number;

  if (imc < 18.5) {
    answer = "Estás en los huesos, deberías comer más";
    num = 1;
  } else if (imc < 25) {
    answer = "Usted está perfectamente normal.";
    num = 2;
  } else if (imc < 30) {
    answer =
      "Como una abuela diría: estás fuertote y hermoso.\n" +
      "La realidad es que tienes sobrepeso :-(";
    num = 3;
  } else {
    answer =
      "Es complicado decirte esto, pero padeces, MÍNIMO, obesidad leve.\n" +
      "Recuerda cuidar esos tobillos y espero que te pongas a dieta.";
    num = 4;
  }
  console.log(imc);

  showMessage(answer, num);

  return answer;
};

const showMessage = (message: string, num: number) => {
  const div = document.createElement("div");
  const h1 = document.createElement("h1");
  const img = document.createElement("img");
  const dataContainer = document.getElementById("principal-div");

  h1.textContent = message;
  h1.style.textAlign = "center";
  div.style.width = "800px";

  switch (num) {
    case 1:
      img.src = "./assets/skinny.jpg";
      break;
    case 2:
      img.src = "./assets/normal.jpg";
      break;
    case 3:
      img.src = "./assets/overweight.jpg";
      break;
    case 4:
      img.src = "./assets/morbid-obesity.jpg";
      break;
  }

  div.appendChild(h1);
  h1.appendChild(img);

  dataContainer.appendChild(div);
};

const handleSubmit = () => {
  const heightNum: number = parseInt(height.value);
  const weightNum: number = parseInt(weight.value);

  const answer = imc(weightNum, heightNum);

  alert(`Usted padece: ${answer}`);
};
