var height = document.querySelector("#height");
var weight = document.querySelector("#weight");
var imc = function (weight, height) {
    var imc = ((weight * 100) / Math.pow(height, 2)) * 100;
    if (isNaN(weight) || isNaN(height)) {
        return "Introduce los datos correctamente, por favor";
    }
    var answer;
    var num;
    if (imc < 18.5) {
        answer = "Estás en los huesos, deberías comer más";
        num = 1;
    }
    else if (imc < 25) {
        answer = "Usted está perfectamente normal.";
        num = 2;
    }
    else if (imc < 30) {
        answer =
            "Como una abuela diría: estás fuertote y hermoso.\n" +
                "La realidad es que tienes sobrepeso :-(";
        num = 3;
    }
    else {
        answer =
            "Es complicado decirte esto, pero padeces, MÍNIMO, obesidad leve.\n" +
                "Recuerda cuidar esos tobillos y espero que te pongas a dieta.";
        num = 4;
    }
    console.log(imc);
    showMessage(answer, num);
    return answer;
};
var showMessage = function (message, num) {
    var div = document.createElement("div");
    var h1 = document.createElement("h1");
    var img = document.createElement("img");
    var dataContainer = document.getElementById("principal-div");
    h1.textContent = message;
    h1.style.textAlign = "center";
    div.style.width = "800px";
    switch (num) {
        case 1:
            img.src = "./assets/skinny.jpg";
            break;
        case 2:
            img.src = "./assets/normal.jpg";
            break;
        case 3:
            img.src = "./assets/overweight.jpg";
            break;
        case 4:
            img.src = "./assets/morbid-obesity.jpg";
            break;
    }
    div.appendChild(h1);
    h1.appendChild(img);
    dataContainer.appendChild(div);
};
var handleSubmit = function () {
    var heightNum = parseInt(height.value);
    var weightNum = parseInt(weight.value);
    var answer = imc(weightNum, heightNum);
    alert("Usted padece: " + answer);
};
